import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSelectModule
} from '@angular/material';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ToastrModule} from 'ngx-toastr';
import {TranslateModule} from '@ngx-translate/core';
import { AuthorizationComponent } from './authorization/authorization.component';

@NgModule({
   declarations: [
      AppComponent,
      AuthorizationComponent
   ],
   imports: [
      FormsModule,
      BrowserModule,
      BrowserAnimationsModule,
      MatInputModule,
      MatFormFieldModule,
      MatSelectModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatCardModule,
      MatDialogModule,
      MatAutocompleteModule,
      ToastrModule.forRoot(),
      TranslateModule.forRoot(),
      NgxSpinnerModule,
      MatIconModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
