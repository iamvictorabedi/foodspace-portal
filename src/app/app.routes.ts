import {Routes} from '@angular/router';

import {AuthenticationRoutes} from './authentication/authentication.routes';
import {DashboardRoutes} from './dashboard/dashboard.routes';


export const routes: Routes = [
  ...AuthenticationRoutes,
  ...DashboardRoutes,
  {path: '**', redirectTo: 'login'}
];
