export class CustomValidation {
  static email(email): any {
    if (email.pristine) {
      return null;
    }

    const EMAIL_REGEXP = /^\S*[@]\S*[.]\S*$/;

    email.markAsTouched();

    if (EMAIL_REGEXP.test(email.value)) {
      return null;
    }

    return {
      invalidEmail: true
    };
  }

  static numeric(input): any {
    if (input.pristine) {
      return null;
    }

    const NUM_REGEXP = /^[0-9]+$/;

    input.markAsTouched();

    if (NUM_REGEXP.test(input.value)) {
      return null;
    }

    return {
      invalidInput: true
    };
  }
}
