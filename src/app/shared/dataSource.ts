import { DataSource } from '@angular/cdk/collections';
import { Observable, of } from 'rxjs';

export class MaterialDataSource extends DataSource<any> {

  /**
   * converts data from api to known datatype of material table.
   *
   */
  constructor(private element: Element[]) {
    super();
  }

  connect(): Observable<Element[]> {
    return of(this.element);
  }

  disconnect() {}
}
