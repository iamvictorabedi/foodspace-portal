import {HttpClient} from '@angular/common/http';
import {ModuleWithProviders, NgModule} from '@angular/core';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: []
  ,
  declarations: [
  ],
  providers: []
  ,
  exports: []
  ,
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
