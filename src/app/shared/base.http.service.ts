import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError as _throw} from 'rxjs';

import {NgxSpinnerService} from 'ngx-spinner';

const HttpMethod = {
  POST: 'post',
  GET: 'get',
  PUT: 'put',
  DELETE: 'delete',
  PATCH: 'patch',
  OPTIONS: 'options'
};

@Injectable()
export class BaseHttpService<T extends any> {

  constructor(
    protected http: HttpClient,
    private spinner: NgxSpinnerService,
  ) { }

  request(method: string, url: string, options?: any): Observable<any> {
    this.spinner.show();

    options = this.setAuthHeaders(options, true);
    options.responseType = 'json';

    return this.http
      .request<T>(method, url, options)
      .pipe(
        map(value => {
          this.spinner.hide();
          console.log(value);
          return value;
        }),
        catchError(this.catchRequestError())
      );
  }

  post(url: string, body: any, options?: any): Observable<any> {
    options = options || {};
    options.body = JSON.stringify(body);

    return this.request(HttpMethod.POST, url, options)
      .pipe(catchError(this.catchRequestError()));
  }

  get(url: string, options?: any): Observable<any> {
    return this.request(HttpMethod.GET, url, options)
      .pipe(catchError(this.catchRequestError()));
  }

  private catchRequestError() {
    return (res: any) => {
      console.log(res);
      this.spinner.hide();
      return _throw(res);
    };
  }

  private setAuthHeaders(options?: any, addContentTypeJsonHeader: boolean = false): any {
    const token = 'eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJtZXNzYWdpbmciLCJncmFudHMiOnsiU3lzdGVtIjoiQXc9PSJ9fQ.hFJ9ImDWsdt42rUE-lRyBRTftfEt89Ev6MUUsht7j6UAVn_XzzQTK5lYXa0giGWR_hOUdjCp4WKK8pw9b1cO7zh6mKF32ep7DvMx0vlF-FFdNqcQ3jlSTz1BVg_fMKsafAHiDlLniNsPZv1HzY-eIXKDEgrCix38bcDUqzR9Alw';
    const headers = addContentTypeJsonHeader ? new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    }) : new HttpHeaders({'Authorization': `Bearer ${token}`});

    if (!options) {
      options = {
        headers: headers
      };
    } else if (!options.headers) {
      options.headers = headers;
    }

    return options;
  }
}
