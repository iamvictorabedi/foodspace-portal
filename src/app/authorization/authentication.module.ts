import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import {AuthenticationService} from '../services/authentication.service';

@NgModule({
  imports: [CommonModule, SharedModule],
  providers: [ AuthenticationService],
  declarations: [LoginComponent, ForgotPasswordComponent],
  exports: [LoginComponent]
})
export class AuthenticationModule {
}
